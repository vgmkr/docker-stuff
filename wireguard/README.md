Based on https://github.com/cmulk/wireguard-docker

Do some improvements to fit my needs

*RUNS ON DEBIAN buster ONLY*


Install 

```bash
docker build -t wireguard-buster .
docker run \
	-it \
	--rm \
	--cap-add sys_module \
	-v /lib/modules:/lib/modules \
	wireguard-buster install-module
```

Run

```bash
docker build -t wireguard-buster .
docker run \
	-it \
	--rm \
	--cap-add sys_module \
	-v /lib/modules:/lib/modules \
	-v /etc/wireguard:/etc/wireguard \
	wireguard-buster
```
