#!/bin/sh
if [ "$1" = "start" ];
    pipework --wait && \
    dnsmasq --interface=eth1 \
            --dhcp-range=192.168.101.20,192.168.101.99,255.255.255.0,1h \
            --dhcp-option=3,192.168.101.1 \
            --dhcp-option=6,192.168.101.3 \
            --server=8.8.8.8 \
            --no-daemon
fi
