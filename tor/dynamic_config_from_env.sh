#!/bin/sh


env | grep "^TOR_" | sed "s/TOR_//g"  | sed "s/=/ /" | tee /etc/tor/torrc

sh -c "$@"