#!/usr/bin/env python3
#
#   CJDNS Autoconfiguration
#
import json
import urllib.request
import subprocess
import os
import sys
import argparse


def check(executable, config):
    """
    Do some regular checks
    1. cjdroute executable exists
    2. Check if config exists otherwise generate one
    3. Check  read & write permitions for config file
    """
    if not os.path.exists(executable):
        print(" ! Couln't find cjdroute under {}. Please set the correct --executable path".format(executable))
        sys.exit(1)

    if not os.path.exists(config):
        print(" ~ Configuration file doesn't exist, generate one")
        subprocess.getoutput("{} --genconf > {}".format(executable, config))

    if not (os.access(config, os.R_OK) and os.access(config, os.W_OK)):
        print(" ! We don't have access to configuration file. Maybe try to use sudo ?")
        sys.exit(1)

    return True

def req(url):
    return urllib.request.urlopen(url).read().decode('utf-8')

def get_conf(executable, config):
    conf = subprocess.getoutput("{} --cleanconf < {}".format(executable, config))
    return json.loads(conf)


def already_configured(conf):
    """
    Check if peers are already configured for any interface.
    Returns True if "connectTo" is empty for all interfaces
    """
    for interface in conf.get("interfaces"):  # UDPInterface / ETHInterface
        for binding in conf["interfaces"][interface]:
            if binding.get("connectTo"):
                return True
    return False


def get_peers(location):
    peers = {
        "ipv6": {},
        "ipv4": {}
    }
    # url = "https://peers.fc00.io/1/location/{}".format(location)
    url = get_location(location)
    response = json.loads( req(url) )
    for result in response.get('result'):
        for key, value in result.items():
            if key != "location":
                # IPv6
                if key.startswith('['):
                    peers["ipv6"][key] = value
                # IPv4
                else:
                    peers["ipv4"][key] = value
    return peers


def get_location(location):
    """
    Returns "peers.fc00.io" URL with location bit set.
    If "location" is not set, uses https://geojs.io/ api to get host's geolocation.
    """
    if not location:
        response = json.loads( req("https://get.geojs.io/v1/ip/geo.json") )
        path = ""
        if response.get("continent_code"):
            path += response["continent_code"] + "/"
        if response.get("city"):
            path += response["city"]
        path = path.lower()
        # if not response.get("city"):
        #     response = json.load(urllib.request.urlopen("https://freegeoip.net/json/"))
        return "https://peers.fc00.io/1/location/{}".format(path)
    else:
        return "https://peers.fc00.io/1/location/{}".format(location)


def save(conf, peers, config):
    conf["interfaces"]["UDPInterface"] = []
    if peers.get("ipv4"):
        conf["interfaces"]["UDPInterface"].append({
            "bind": '0.0.0.0:46810',
            "connectTo": peers["ipv4"]
        })
    if peers.get("ipv6"):
        conf["interfaces"]["UDPInterface"].append({
            "bind": '[::]:46810',
            "connectTo": peers["ipv6"]
        })
    with open(config, "w") as f:
        f.write(json.dumps(conf, indent=2))


def main(args):
    if not os.path.exists(args.config):
        check(args.executable, args.config)
        conf = get_conf(args.executable, args.config)
    else:
        conf = json.load(open(args.config))
    if not already_configured(conf):
        peers = get_peers(args.location)
        save(conf, peers, args.config)
    sys.exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='Hyperobia initialize')
    parser.add_argument("-c", "--config",     default="/etc/cjdroute.conf",
                        help="CJDNS configuration path (default: /etc/cjdroute.conf)")
    parser.add_argument("-e", "--executable", default="/usr/bin/cjdroute",
                        help="CJDNS executable path (default: /usr/bin/cjdroute)")
    parser.add_argument("-l", "--location",   default="",
                        help="Location to search peers from e.g.: 'uk/london'. If empty will try to geolocate this mashine. (default:'')")
    args = parser.parse_args()
    main(args)
