#!/usr/bin/env python3
import asyncio
import aiohttp
from proxybroker import Broker, ProxyPool
from proxybroker.errors import NoProxyError

async def verbose(proxies):
    while True:
        proxy = await proxies.get()
        if proxy is None:
            break
        else:
            print( ' ~ pulled proxy {}:{}'.format(proxy.host, proxy.port) )

@asyncio.coroutine
def cron():
    while True:
        # broker.show_stats(verbose=False)
        yield from asyncio.sleep(60)

host, port = '0.0.0.0', 8888
serverloop = asyncio.get_event_loop()
types      = ['HTTPS']
proxies    = asyncio.Queue(loop=serverloop)
broker     = Broker(queue=proxies, max_tries=1, loop=serverloop)
broker.serve(host=host, port=port, types=types, limit=5,
            max_tries=1, strict=True,
            min_req_proxy=5, max_error_rate=0.5,
            max_resp_time=8, backlog=10, queue=proxies)

#loop.run_until_complete(cron())

import threading
def loop_in_thread(loop):
    asyncio.set_event_loop(loop)
    loop.run_until_complete(cron())

import os
import signal

class Srelay(object):
    """docstring for Srelay"""
    def __init__(self, arg={}):
        super(Srelay, self).__init__()
        self.arg = {}
        self.arg["pid"]    = arg.get('pid',    '/tmp/srelay.pid')
        self.arg["config"] = arg.get('config', '/tmp/srelay.conf')
        self.arg["host"]   = arg.get('host',   '0.0.0.0')
        self.arg["port"]   = arg.get('port',   8888)
        self.arg["loc"]    = arg.get('loc',    '/usr/bin/srelay')
    def start(self):
        os.system("{loc} -i {host}:{port} -c {config} -p {pid}".format(**self.arg) )
    def stop(self):
        if os.path.exists(self.arg['pid']):
            with open(self.arg['pid']) as f:
                pid=int(f.read().strip())
                os.kill(pid, signal.SIGTERM)
    def restart(self):
        self.stop()
        self.start()
    def config(self, proxies=[{'type':'SOCKS5','host':"127.0.0.1",'port':9050}]):
        # # dest    dest-port    next-hop        next-port  nnext-hop  nnext-port
        # 0.0.0.0   any          109.120.166.99  8080       10.66.6.1  9050    
        conf = ['0.0.0.0', 'any']
        for proxy in proxies:
            proxy['type'] = proxy.get('type').lower()[0]
            conf.append("{host} {port}/{type}".format(**proxy))
        with open(self.arg['config'], 'w') as f:
            f.write( ' '.join(conf) )
        
def srelay(loop):


t = threading.Thread(target=loop_in_thread, args=(serverloop,))
t.start()


broker = Broker(proxies)
tasks  = asyncio.gather(broker.find(countries=['GB'], limit=10),verbose(proxies))
secondloop = asyncio.get_event_loop()
secondloop.run_until_complete(tasks)


# @asyncio.coroutine
# def cron():
#     while True:
#         broker.show_stats(verbose=False)
#         yield from asyncio.sleep(2)
# broker     = Broker(max_tries=1, loop=loop)
# # Broker.serve() also supports all arguments that are accepted
# # Broker.find() method: data, countries, post, strict, dnsbl.
# broker.serve(host=host, port=port, types=types, limit=5, max_tries=3,
#             min_req_proxy=5, max_error_rate=0.5,
#             max_resp_time=8, backlog=10)
# loop.run_until_complete(cron())
# broker.stop()
