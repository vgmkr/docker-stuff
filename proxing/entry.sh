#!/bin/sh

mkdir -p /data/proxies

if [ "$1" = "http" ];then
    curl --stderr /dev/null https://proxy.l337.tech/txt | tee /data/proxies/http.txt
elif [ "$1" = "socks5" ];then
    curl --stderr /dev/null https://proxy.l337.tech/txt | tail -2 |\
        xargs -I {} sh -c '
            curl --stderr /dev/null \
                --proxy http://{} \
                "http://pubproxy.com/api/proxy?limit=5&format=txt&type=socks5";
            printf "\n";
        ' |\
    tee /data/proxies/socks5.txt
else
    exec "$@"
fi
