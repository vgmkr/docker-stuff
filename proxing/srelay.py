#!/usr/bin/env python3
import asyncio
from proxybroker import Broker
import os
import signal
import schedule
import time
import random

async def load(queue, proxies):
    while True:
        proxy = await queue.get()
        if proxy is None:
            break
        else:
            proto = ''
            if 'HTTPS' in proxy.types:
                proto = 'HTTPS'
            elif 'SOCKS5' in proxy.types:
                proto = 'SOCKS5'
            elif 'SOCKS4' in proxy.types:
                proto = 'SOCKS4'
            else:
                proto = 'HTTP'
            proxies.append({
                "proto": proto,
                "host": proxy.host,
                "port": proxy.port
            })

class Srelay(object):
    """docstring for Srelay"""
    def __init__(self, arg={}):
        super(Srelay, self).__init__()
        self.arg = {}
        self.arg["pid"]    = arg.get('pid',    '/tmp/srelay.pid')
        self.arg["config"] = arg.get('config', '/tmp/srelay.conf')
        self.arg["host"]   = arg.get('host',   '0.0.0.0')
        self.arg["port"]   = arg.get('port',   8888)
        self.arg["loc"]    = arg.get('loc',    '/usr/bin/srelay')
        self.proxies       = []
        self.pre_proxy     = {'proto':'SOCKS5','host':"10.66.6.1",'port':9050}
        self.running       = True
    def start(self):
        os.system("{loc} -i {host}:{port} -c {config} -p {pid}".format(**self.arg) )
    def stop(self):
        if os.path.exists(self.arg['pid']):
            with open(self.arg['pid']) as f:
                pid=int(f.read().strip())
                os.kill(pid, signal.SIGTERM)
    def restart(self):
        self.stop()
        self.start()
    def config(self):
        # # dest    dest-port    next-hop        next-port  nnext-hop  nnext-port
        # 0.0.0.0   any          109.120.166.99  8080       10.66.6.1  9050
        conf  = ['0.0.0.0', 'any']
        proxy = random.choice(self.proxies)
        proxy['proto'] = proxy.get('proto').lower()[0]
        conf.append("{host} {port}/{proto}".format(**proxy))
        self.pre_proxy['proto'] = self.pre_proxy.get('proto').lower()[0]
        conf.append("{host} {port}/{proto}".format(**self.pre_proxy))
        with open(self.arg['config'], 'w') as f:
            f.write( ' '.join(conf) )
    def load(self):
        self.proxies = []
        queue   = asyncio.Queue()
        broker  = Broker(queue)
        tasks   = asyncio.gather(
            broker.find(types=['SOCKS5'], limit=10),
            load(queue, self.proxies)
        )
        loop = asyncio.get_event_loop()
        loop.run_until_complete(tasks)
        print(' + Loaded {} proxies'.format(len(self.proxies)))
    def reload(self):
        self.load()
        self.config()
        self.restart()
    def run(self):
        schedule.every(1).minutes.do( self.reload )
        while self.running:
            schedule.run_pending()
            time.sleep(1)
        self.stop()

def main():
    relay = Srelay()
    relay.reload()
    relay.run()

if __name__ == '__main__':
    main()
