#!/bin/sh

mkdir -p /data/proxies

goodproxy(){
    TYPE=$1
    echo "" > /data/proxies/$TYPE.txt
    echo " ~ REAL IP : $( curl --stderr /dev/null http://icanhazip.com)"
    proxybroker find --types $TYPE --limit 10 2> /dev/null |\
        awk '{print $NF}' | tr -d '>' | tee /tmp/$TYPE.txt
        # grep -oP "(?<=\[$TYPE\] )(.*)(?=>)" | tee /tmp/$TYPE.txt
    while read line;do
        echo " ~ TESTING $line"
        curl --connect-timeout 5 \
            --proxy $TYPE://$line \
            --stderr /dev/null http://icanhazip.com 1> /dev/null &&
        echo $line | tee -a /data/proxies/$TYPE.txt 1> /dev/null 
    done < /tmp/$TYPE.txt
}

if [ "$1" = "HTTP" ];then
    goodproxy HTTP
elif [ "$1" = "HTTPS" ];then
    goodproxy HTTPS
elif [ "$1" = "SOCKS5" ];then
    goodproxy SOCKS5
else
    exec "$@"
fi
