#!/bin/bash
# entry.sh

[ -n "$HUNTER_KEY" ] &&\
    sed -i "s/self.key = \"\"/self.key = \"${HUNTER_KEY}\"/" /usr/share/theHarvester/discovery/huntersearch.py

/usr/share/theHarvester/theHarvester.py $@