# theHarvester 

This is a docker image building theHarvester (https://github.com/laramies/theHarvester
) 

Use:

```bash
docker build -t harvest .
docker run --rm -it --env HUNTER_KEY=... harvest -d google.com -b all
```