#!/bin/bash
#entry.sh
set -e

if [ "$1" = "start" ];
then 

    ### Webmin
    # set password
    echo "root:${WEBMIN_PASS}" | chpasswd
    # remove bind directive if exists
    if grep "^bind=" /etc/webmin/miniserv.conf;then
        grep -v "^bind=" /etc/webmin/miniserv.conf > /tmp/temp;
        mv /tmp/temp /etc/webmin/miniserv.conf
    fi
    # add bind directive
    echo "bind=${WEBMIN_BIND}" | tee -a /etc/webmin/miniserv.conf
    # start webmin
    /etc/init.d/webmin start

    # Bind
    mkdir -m 0775 -p /var/cache/bind
    chown -R root:bind /var/cache/bind
    chown -R root:bind /etc/bind
    # Test configuration
    /usr/sbin/named-checkconf /etc/bind/named.conf || exit 1
    /usr/sbin/named-checkzone \
        road.war \
        /etc/bind/zones/road.war.zone || exit 1
    /usr/sbin/named-checkzone \
        66.10.in-addr.arpa. \
        /etc/bind/zones/66.10.in-addr.arpa.zone || exit 1
    
    pipework --wait &&\
        /usr/sbin/named -g -c /etc/bind/named.conf

else
    exec "$@"
fi

