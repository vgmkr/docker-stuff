#!/bin/bash
#entry.sh
set -e

if [ "$1" = "start" ];
then
    
    # Bind
    mkdir -m 0775 -p /var/cache/bind
    chown -R root:bind /var/cache/bind
    chown -R root:bind /etc/bind
    # Test configuration
    /usr/sbin/named-checkconf /etc/bind/named.conf || exit 1
    pipework --wait &&\
        /usr/sbin/named -g -c /etc/bind/named.conf

else
    exec "$@"
fi

