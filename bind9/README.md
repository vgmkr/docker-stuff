
# BIND DNS

The setup consists of a master and a slave Bind node.
Both servers serves `road.war` zone and run on docker

 
# Usage

- Install
    - Assuming docker and docker-compose are already installed
    - then run : `start.sh`
- Add domain
    - Create A record
        ```
        nsupdate -y rndckey:4qaxED8F/76MmOd6bHjHcw== -v <<EOF
        server 10.66.6.101
        zone road.war.
        update delete whatever.road.war. A
        update add whatever.road.war. 30 A 10.66.6.101
        send
        show
        EOF
        ```
    - Test it on both master and slave server
        ```
        dig @10.66.6.101 whatever.road.war
        dig @10.66.6.102 whatever.road.war
        ```

# TODO
- follow instructions and setup public as well as private DNS server
    https://www.howtoforge.com/two_in_one_dns_bind9_views

