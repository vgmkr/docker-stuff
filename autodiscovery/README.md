README
======

Inspired from https://github.com/jderusse/docker-dns-gen

Dynamic Dns Server and Registrator 

```bash
docker build -t dns .
```

```bash
docker run \
    -it \
    --rm \
    --label service=dns \
    --name registrator \
    --env DOMAIN=road.war \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    -v /tmp/dnsmasq.d:/etc/dnsmasq.d \
    dns
```



```bash
drill @127.0.0.1 dnsmasq.road.war | grep -v '^;\|^$'
```