#!/usr/bin/env python
"""

> NAME: multi-hook.py

> DESCRIPTION:
    hook to help with let's encrypt certificates

> DOCUMENTATION:
    https://certbot.eff.org/docs/using.html#pre-and-post-validation-hooks

> INSTALLATION:
    sudo apt-get install    -y software-properties-common
    sudo add-apt-repository -y ppa:certbot/certbot
    sudo apt-get update
    sudo apt-get install    -y certbot

> USAGE:
    pip install awscli certbot

    export AWS_DEFAULT_REGION=us-west-1
    export AWS_ACCESS_KEY_ID=XXXXXXXXXX
    export AWS_SECRET_ACCESS_KEY=XXXXXXXXXX
    certbot certonly \
            --manual \
            --preferred-challenges=dns \
            --manual-auth-hook '/usr/bin/multi-hook.py auth' \
            --manual-cleanup-hook '/usr/bin/multi-hook.py cleanup' \
            -d <YOUR DOMAIN>
"""
import os
import sys
import boto3
from time import sleep
import json

if len(sys.argv) != 2:
    print(" ! [multi-hook][ERROR] Please spesify an argument")
    print(" ! [multi-hook][ERROR] {} <arg> where argument can be auth, clean".format(sys.argv[0]))
    sys.exit(1)

def gethostedzone(client, domain):
    resp = client.list_hosted_zones()
    for zone in resp['HostedZones']:
        if zone['Name'][:-1] in domain:
            return zone['Id']
    return None

def setup_dns(client, hosted_zone_id, domain, txt_challenge):
    resp = client.change_resource_record_sets(
        HostedZoneId=hosted_zone_id,
        ChangeBatch={
            'Changes': [{
                'Action': 'UPSERT',
                'ResourceRecordSet': {
                    'Name': '_acme-challenge.{0}'.format(domain),
                    'Type': 'TXT',
                    'TTL': 5,
                    'ResourceRecords': [{
                        'Value': '"{0}"'.format(txt_challenge)
                    }]
                }
            }]
        }
    )
    sleep(30)

def delete_dns(client, hosted_zone_id, domain, txt_challenge):
    resp = client.change_resource_record_sets(
        HostedZoneId=hosted_zone_id,
        ChangeBatch={
            'Changes': [{
                'Action': 'DELETE',
                'ResourceRecordSet': {
                    'Name': '_acme-challenge.{0}'.format(domain),
                    'Type': 'TXT',
                    'TTL': 5,
                    'ResourceRecords': [{
                        'Value': '"{0}"'.format(txt_challenge)
                    }]
                }
            }]
        }
    )

if __name__ == "__main__":

    CERTBOT_DOMAIN     = os.environ['CERTBOT_DOMAIN']
    CERTBOT_VALIDATION = os.environ['CERTBOT_VALIDATION']

    action             = sys.argv[1]
    session            = boto3.Session()
    client             = session.client("route53")
    zone_id            = gethostedzone(client, CERTBOT_DOMAIN)

    if not zone_id:
        print(' ! [multi-hook][{}][ERROR] We were unable to find AWS hosted zone for provided domain "{}" '.format(action.upper(), CERTBOT_DOMAIN))
        sys.exit(1)

    if action == "auth":
        print(' ~ [multi-hook][{}][INFO] Creating DNS record for "_acme-challenge.{}" '.format(action.upper(), CERTBOT_DOMAIN))
        setup_dns(client, zone_id, CERTBOT_DOMAIN, CERTBOT_VALIDATION)
    elif action == "cleanup":
        print(' ~ [multi-hook][{}][INFO] Cleaning DNS record for "_acme-challenge.{}" '.format(action.upper(), CERTBOT_DOMAIN))
        delete_dns(client, zone_id, CERTBOT_DOMAIN, CERTBOT_VALIDATION)
    else:
        print(' ! [multi-hook][UNEXPECTED][ERROR] Unknown ACTION')
    sys.exit(0)
