#!/bin/sh

DOMAIN=$1

[ -n "$DOMAIN" ] || echo "[ERROR] Argument domain is not missing"
[ -n "$DOMAIN" ] || echo "[USAGE] $0 <domain>"
[ -n "$DOMAIN" ] || exit 1

# Check for variables
for variable in AWS_DEFAULT_REGION \
                AWS_ACCESS_KEY_ID \
                AWS_SECRET_ACCESS_KEY \
                TOS_EMAIL;
do
  env | grep -q ${variable} || echo "[ERROR] ${variable} is not set"
  env | grep -q ${variable} || exit 1
done

# Create certificate
certbot certonly \
        --agree-tos \
        --non-interactive \
        --email ${TOS_EMAIL} \
        --manual-public-ip-logging-ok \
        --preferred-challenges dns \
        --manual \
        --manual-auth-hook '/usr/bin/multi-hook.py auth' \
        --manual-cleanup-hook '/usr/bin/multi-hook.py cleanup' \
        -d ${DOMAIN}
