# Tahoe-Lafs


# DESIGN

Tahoe-Lafs componets will be deployed in
    - Each public node will run an introducer
    - Each public node will run a client and [SFTP Frontends][1]
    - Each public and router node will share a system folder a.k.a [magic-folder][2]
    - Add [backups][5] for shared magic-folder
    - Any node can run Server storage as long as they are constantly connected
    - Any node can run Client
    - Possible run [helpers][3] on public nodes as they supposed to be faster than shitty home ISPs
    - [*server_of_happiness*][4] and [*N*][4] and [*k*][4] factors 
        - We run a small network and hope to expand it in the future
        - servers_of_happiness = smallest number of servers that you expect to have online
        - N = number of storage servers 
        - N/k = Desired availability (default 3.3 N=10/k=3)
    - Use Tahoe as [key-value store][6]  



[1]: https://tahoe-lafs.readthedocs.io/en/latest/frontends/FTP-and-SFTP.html
[2]: https://tahoe-lafs.readthedocs.io/en/latest/proposed/magic-folder/user-interface-design.html
[3]: https://tahoe-lafs.readthedocs.io/en/latest/helper.html
[4]: https://tahoe-lafs.readthedocs.io/en/latest/architecture.html
[5]: https://tahoe-lafs.readthedocs.io/en/latest/backupdb.html
[6]: https://tahoe-lafs.readthedocs.io/en/latest/key-value-store.html


INSTALL https://tahoe-lafs.readthedocs.io/en/latest/INSTALL.html#first-in-case-of-trouble

INTRODUSER https://tahoe-lafs.readthedocs.io/en/latest/configuration.html#additional-introducer-definitions

https://www.linode.com/docs/applications/cloud-storage/tahoe-lafs-on-debian-9/#install-tahoe-lafs-and-set-up-the-introducer

```
docker build -t registry.road.war/tahoe-lafs .
docker push registry.road.war/tahoe-lafs
```

0. Load IP

```
export IFACE=$(ip r show default | cut -d ' ' -f 5)
export ADDR=$(ip -4 a show $IFACE | grep -oP '(?<=inet )(.*)(?=/)' )
```

1. Start introducer

```
docker run --rm -dit \
    -e INTRODUCER_NAME=introducer01 \
    -e INTRODUCER_LOCATION=tcp:$ADDR:1234 \
    -e INTRODUCER_PORT=tcp:1234 \
    --publish "$ADDR:1234:1234" \
    --volume  "`pwd`/introducer01:/data/introducer01" \
    registry.road.war/tahoe-lafs introducer
```

2. Get introducer url

`export INTRODUCER_FURL=$(sudo cat introducer01/private/introducer.furl)`

3. Start server

```
docker run --rm -dit \
    -e NODE_NAME=node01 \
    -e NODE_LOCATION=tcp:$ADDR:1235 \
    -e NODE_PORT=tcp:1235 \
    -e INTRODUCER_NAME=introducer01 \
    -e INTRODUCER_FURL=$INTRODUCER_FURL \
    --publish "$ADDR:1235:1235" \
    --volume  "`pwd`/node01:/data/node01" \
    registry.road.war/tahoe-lafs server
```

3. Start client

```
docker run --rm -dit \
    -e CLIENT_NAME=client01 \
    -e CLIENT_WEBPORT=tcp:3456:interface=0.0.0.0 \
    -e INTRODUCER_NAME=introducer01 \
    -e INTRODUCER_FURL=$INTRODUCER_FURL \
    --publish "$ADDR:3456:3456" \
    --volume  "`pwd`/client01:/data/client01" \
    registry.road.war/tahoe-lafs client
```