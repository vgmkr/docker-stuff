#!/bin/sh

mkdir -p /data

add_introducer(){
    
    BASEDIR=$1

    # If introducer exists add it in private/introducers.yaml
    if grep -q ${INTRODUCER_FURL} $BASEDIR/tahoe.cfg;
    then
        # Create file if doesn't exists
        [ ! -f $BASEDIR/private/introducers.yaml ] && \
            echo 'introducers:' | tee $BASEDIR/private/introducers.yaml
        
        # Add introducer if doesn't exists
        if ! grep -q ${INTRODUCER_FURL} $BASEDIR/tahoe.cfg;
        then
            echo "  ${INTRODUCER_NAME}:"| tee -a $BASEDIR/private/introducers.yaml
            echo "    furl: ${INTRODUCER_FURL}"| tee -a $BASEDIR/private/introducers.yaml
        fi

    fi

}


if [ "$1" = "introducer" ];
then
    
    if [ ! -f /data/${INTRODUCER_NAME}/private/introducer.furl ];
    then
        /usr/local/bin/tahoe create-introducer \
            --port=${INTRODUCER_PORT} \
            --location=${INTRODUCER_LOCATION} \
            --basedir=/data/${INTRODUCER_NAME}
    fi
    /usr/local/bin/tahoe run --basedir /data/${INTRODUCER_NAME}

elif [ "$1" = "server" ];
then

    if [ ! -f /data/${NODE_NAME}/private/storage.furl ];
    then
        /usr/local/bin/tahoe create-node \
            --nickname=${NODE_NAME} \
            --introducer=${INTRODUCER_FURL} \
            --port=${NODE_PORT} \
            --location=${NODE_LOCATION} \
            --basedir=/data/${NODE_NAME}
    fi
    add_introducer ${INTRODUCER_FURL}
    /usr/local/bin/tahoe run --basedir /data/${NODE_NAME}

elif [ "$1" = "client" ];
then

    if [ ! -f /data/${CLIENT_NAME}/private/secret ];
    then
        /usr/local/bin/tahoe create-client \
            --nickname=${CLIENT_NAME} \
            --introducer=${INTRODUCER_FURL} \
            --basedir=/data/${CLIENT_NAME} \
            --webport=${CLIENT_WEBPORT}
    fi
    add_introducer ${INTRODUCER_FURL}
    /usr/local/bin/tahoe run --basedir /data/${CLIENT_NAME}

else
    exec "$@"
fi
