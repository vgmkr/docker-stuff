#!/bin/sh


docker build -t registry.gitlab.com/vgmkr/docker-stuff:dnsmasq ./dnsmasq
docker push registry.gitlab.com/vgmkr/docker-stuff:dnsmasq

docker build -t registry.gitlab.com/vgmkr/docker-stuff:zeronet ./zeronet
docker push registry.gitlab.com/vgmkr/docker-stuff:zeronet

docker build -t registry.gitlab.com/vgmkr/docker-stuff:tor ./tor
docker push registry.gitlab.com/vgmkr/docker-stuff:tor


git clone https://github.com/ether/etherpad-lite
cd etherpad-lite/docker
docker build -t registry.gitlab.com/vgmkr/docker-stuff:etherpad .
docker push registry.gitlab.com/vgmkr/docker-stuff:etherpad
cd ../../
rm -r etherpad-lite

docker build -t registry.gitlab.com/vgmkr/docker-stuff:recon-ng ./recon-ng
docker push registry.gitlab.com/vgmkr/docker-stuff:recon-ng

docker build -t registry.gitlab.com/vgmkr/docker-stuff:nfs ./nfs
docker push registry.gitlab.com/vgmkr/docker-stuff:nfs
