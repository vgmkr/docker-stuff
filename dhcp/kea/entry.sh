#!/bin/sh

if [ "$1" = "dhcp4" ];
then 

    /usr/bin/kea-dhcp4 -t /etc/kea/kea.conf         || exit 1
    /usr/bin/kea-dhcp4 -d -c /etc/kea/kea.conf      || exit 1

elif [  "$1" = "dhcp-ddns" ];
then

    /usr/bin/kea-dhcp-ddns -t /etc/kea/kea.conf     || exit 1
    /usr/bin/kea-dhcp-ddns -d -c /etc/kea/kea.conf  || exit 1

else
    exec "$@"

fi